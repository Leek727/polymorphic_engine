"""
polymorphic engine inside encrypted body

sprints
    1. poly engine
        - take a string a encode it with random bit operations
        - generate the corresponding decoder
        - read itself as input

Bit operations
~	Bitwise NOT	~x           # ~x
^	Bitwise XOR	x ^ y        # (x ^ y) ^ y
>>	Bitwise right shift	x>>  # x << y >> y
<<	Bitwise left shift	x<<
"""
import random
import __main__ as main

body = ""
with open(main.__file__, "r") as f:
    body = f.read()
    
# ------------------------------- polymorphic engine  ---------------------------------
# encoding     decoding
operators = {
    "~x"    :  "~x",
    "(x^1)" :  "x^1",
    "x<<1"  :  "x>>1"
}

# a list of operations
key = []
for i in range(11):
    key.append(random.choice(list(operators.keys())))

# apply each operation to a single byte
c_op = 0 # pointer to current operation
encoded = [str(ord(x)) for x in body]
for operation in key:
    for i,char in enumerate(list(encoded)):
        encoded[i] = str(eval(operation.replace("x", char)))

encoded = [hex(int(x)) for x in encoded]

# decoding loop
template = """
output = []
for i in list(enc_body):
    x = i
"""
for operation in key[::-1]:
    template += "    x = " + operators[operation] + '\n'

template = f"enc_body = [{','.join(encoded)}]\n" + template + "    output.append(x)" + "\nprint(''.join(chr(x) for x in output))"

with open("notavirus2.py", "w") as f:
    f.write(template)

#eval(compile(template, '<string>', 'exec'))

#encoded = list([int(x,16) for x in encoded])
